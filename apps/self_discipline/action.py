# coding: utf8

"""动态执行配置的不同"鼓励项"动作对应的语音指令"""

import datetime
import random
from apps.self_discipline.db import s3db
from yiwa.browser import refresh
from yiwa.log import Log

logger = Log("self_discipline.log").logger


def encourage(driver, option):
    """
    给予鼓励，可以适应每一个鼓励项（传入鼓励项名称），不必每个鼓励项开发一个方法
    :param driver:  浏览器
    :param option:  鼓励项
    :return:    None
    """
    # day表插一条
    sqlQueryOption = f"""
        SELECT id FROM options
        WHERE name='{option}';
    """
    option = s3db.select(sqlQueryOption)
    if not option:
        logger.error(f"鼓励项不存在：{option}")
        return None
    optionID, = option[0]  # (1, )
    week = datetime.datetime.now().weekday()
    date = datetime.date.today().__str__()
    createTime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    def _randomScore():
        """随机一个奖励分值id"""
        _sql = "SELECT id FROM scores;"
        _scores = s3db.select(_sql)
        if not _scores:
            return 0
        _score = random.choice(_scores)
        return _score[0]
    sqlInsertDay = f"""
        INSERT INTO day(`option`, `score`, `date`, `week`, `createtime`)
        VALUES({optionID}, {_randomScore()}, '{date}', {week}, '{createTime}');
    """
    s3db.execute(sqlInsertDay)

    # 余额加一
    _sqlSelect = "SELECT * FROM exchange_records WHERE last=1;"
    _records = s3db.select(_sqlSelect)
    if not _records:
        _sqlInsert = "INSERT INTO exchange_records(balance, last) VALUES(1, 1);"
        s3db.execute(_sqlInsert)
    else:
        _sqlUpdate = "UPDATE exchange_records SET balance=balance+1 WHERE last=1;"
        s3db.execute(_sqlUpdate)

    # 刷新页面
    refresh(driver)


if __name__ == "__main__":
    encourage(object, "按时起床")
